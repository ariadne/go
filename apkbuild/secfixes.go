package apkbuild

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strings"

	"gopkg.in/yaml.v2"
)

type secfixesData struct {
	Secfixes yaml.MapSlice
}

type FixedVersion struct {
	Version string
	Fixes   []Fix
	LineNr  uint
}

type Fix struct {
	Identifiers []string
	LineNr      uint
}

type Secfixes []FixedVersion

func (s Secfixes) Get(version string) *FixedVersion {
	for _, fixedVersion := range s {
		if fixedVersion.Version == version {
			return &fixedVersion
		}
	}

	return nil
}

func ParseSecfixes(apkbuild io.ReadCloser) (secfixes Secfixes, err error) {
	defer apkbuild.Close()
	apkbuildScanner := bufio.NewScanner(apkbuild)

	var apkbuildLineNr uint
	var lineNr uint
	secfixYaml := bytes.Buffer{}
	inSecFixes := false

scanLoop:
	for apkbuildScanner.Scan() {
		line := apkbuildScanner.Text()
		apkbuildLineNr++

		switch {
		case inSecFixes && (len(line) == 0 || line[0:1] != "#"):
			break scanLoop
		case line == "# secfixes:":
			lineNr = apkbuildLineNr
			inSecFixes = true
			fallthrough
		case inSecFixes:
			if len(line) > 1 {
				secfixYaml.WriteString(line[2:])
			}
			secfixYaml.WriteByte('\n')
		}
	}

	secfixesData := secfixesData{}
	err = yaml.Unmarshal(secfixYaml.Bytes(), &secfixesData)

	if err != nil {
		return
	}

	for _, entry := range secfixesData.Secfixes {
		lineNr++
		secfixVersion := FixedVersion{
			Version: fmt.Sprintf("%v", entry.Key),
			LineNr:  lineNr,
		}

		fixEntries := entry.Value.([]interface{})
		for _, entry := range fixEntries {
			lineNr++
			secfix := Fix{
				Identifiers: strings.Split(entry.(string), " "),
				LineNr:      lineNr,
			}

			secfixVersion.Fixes = append(secfixVersion.Fixes, secfix)
		}
		secfixes = append(secfixes, secfixVersion)
	}

	return
}
