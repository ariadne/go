package apkbuild

import (
	"strings"
	"testing"

	"github.com/MakeNowJust/heredoc"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"mvdan.cc/sh/v3/expand"
)

func TestParserDoesNotError(t *testing.T) {
	file := strings.NewReader("")

	_, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	assert.Nil(t, err)
}

func TestParserExtractsBasicInfo(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname=test-package
		pkgver=1.2.3
		pkgrel=4
		pkgdesc="A test package"
		url="https://domain.org"
		license="MIT"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	assert.Equal(t, "test-package", apkbuild.Pkgname)
	assert.Equal(t, "1.2.3", apkbuild.Pkgver)
	assert.Equal(t, "4", apkbuild.Pkgrel)
	assert.Equal(t, "A test package", apkbuild.Pkgdesc)
	assert.Equal(t, "https://domain.org", apkbuild.Url)
	assert.Equal(t, "MIT", apkbuild.License)
}

func TestParserExtractsOptions(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		options="net !check"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	assert.True(t, apkbuild.Options.Has("net"), "Expecting options to include 'test'")
	assert.True(t, apkbuild.Options.Has("!check"), "Expecting options to include '!check'")
	assert.False(t, apkbuild.Options.Has("suid"), "Expecting options to not include 'suid'")
}

func TestParserExtractsDependencies(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		depends="package-a package-b>=2.0 !package-c"
		makedepends="foo-dev"
		checkdepends="py3-pytest"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.Len(t, apkbuild.Depends, 3, "Expected apkbuild.Depends to have 3 elements")
	assert.Equal(t, "package-a", apkbuild.Depends[0].Pkgname)
	assert.Equal(t, "package-b", apkbuild.Depends[1].Pkgname)
	assert.Equal(t, "2.0", apkbuild.Depends[1].Version)
	assert.Equal(t, ">=", apkbuild.Depends[1].Constraint)
	assert.Equal(t, "package-c", apkbuild.Depends[2].Pkgname)
	assert.True(t, apkbuild.Depends[2].Conflicts)

	require.Len(t, apkbuild.Makedepends, 1, "Expected apkbuild.Makedepends to have 1 element")
	require.Equal(t, "foo-dev", apkbuild.Makedepends[0].Pkgname)

	require.Len(t, apkbuild.Checkdepends, 1, "Expected apkbuild.Checkdepends to have 1 element")
	require.Equal(t, "py3-pytest", apkbuild.Checkdepends[0].Pkgname)
}

func TestParserExtractsProvides(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		provides="other-package=1.2.3-r1"
		provider_priority="100"
		replaces="other-package"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.Len(t, apkbuild.Provides, 1)
	assert.Equal(t, "other-package", apkbuild.Provides[0].Pkgname)
	assert.Equal(t, "=", apkbuild.Provides[0].Constraint)
	assert.Equal(t, "1.2.3-r1", apkbuild.Provides[0].Version)

	assert.Equal(t, "100", apkbuild.ProviderPriority)

	require.Len(t, apkbuild.Replaces, 1)
	assert.Equal(t, "other-package", apkbuild.Replaces[0].Pkgname)
}

func TestParserExtractsSimplesourceUrl(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		source="https://domain.org/project/project-v1.2.3.tar.gz"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.Len(t, apkbuild.Source, 1)
	assert.Equal(t, "project-v1.2.3.tar.gz", apkbuild.Source[0].Filename)
	assert.Equal(t, "https://domain.org/project/project-v1.2.3.tar.gz", apkbuild.Source[0].Location)
}

func TestParserExtractsUrlWithNamedFile(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		pkgver="1.2.3"
		source="test-package-$pkgver.tar.gz::https://github.com/foo/test/archive/refs/tags/v$pkgver.tar.gz"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.Len(t, apkbuild.Source, 1)
	assert.Equal(t, "test-package-1.2.3.tar.gz", apkbuild.Source[0].Filename)
	assert.Equal(t, "https://github.com/foo/test/archive/refs/tags/v1.2.3.tar.gz", apkbuild.Source[0].Location)
}

func TestParserExtractsLocalFile(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		pkgver="1.2.3"
		source="fix-build.patch"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.Len(t, apkbuild.Source, 1)
	assert.Equal(t, "fix-build.patch", apkbuild.Source[0].Filename)
	assert.Equal(t, "fix-build.patch", apkbuild.Source[0].Location)
}

func TestParserExtractsMultipleSources(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		pkgver="1.2.3"
		source="https://domain.org/project/project-v1.2.3.tar.gz
				fix-build.patch
				"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.Len(t, apkbuild.Source, 2)
	assert.Equal(t, apkbuild.Source[0].Filename, "project-v1.2.3.tar.gz")
	assert.Equal(t, apkbuild.Source[1].Filename, "fix-build.patch")
}

func TestParserParsesSimpleSubpackage(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		pkgver="1.2.3"
		subpackages="$pkgname-component"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.Len(t, apkbuild.Subpackages, 1)
	assert.Equal(t, "test-package-component", apkbuild.Subpackages[0].Subpkgname)
	assert.Empty(t, apkbuild.Subpackages[0].SplitFunc)
	assert.Empty(t, apkbuild.Subpackages[0].Arch)
}

func TestParserParsesSubpackageWithSplitFunc(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		pkgver="1.2.3"
		subpackages="$pkgname-component-foo:foo"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.Len(t, apkbuild.Subpackages, 1)
	assert.Equal(t, "test-package-component-foo", apkbuild.Subpackages[0].Subpkgname)
	assert.Equal(t, "foo", apkbuild.Subpackages[0].SplitFunc)
	assert.Empty(t, apkbuild.Subpackages[0].Arch)
}

func TestParserParsesSubpackageWithSplitFuncAndArch(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		pkgver="1.2.3"
		subpackages="$pkgname-component-foo:foo:noarch"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.Len(t, apkbuild.Subpackages, 1)
	assert.Equal(t, "test-package-component-foo", apkbuild.Subpackages[0].Subpkgname)
	assert.Equal(t, "foo", apkbuild.Subpackages[0].SplitFunc)
	assert.Equal(t, "noarch", apkbuild.Subpackages[0].Arch)
}

func TestParserParsesSubpackageWithArch(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		pkgver="1.2.3"
		subpackages="$pkgname-component-foo::noarch"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.Len(t, apkbuild.Subpackages, 1)
	assert.Equal(t, "test-package-component-foo", apkbuild.Subpackages[0].Subpkgname)
	assert.Empty(t, apkbuild.Subpackages[0].SplitFunc)
	assert.Equal(t, "noarch", apkbuild.Subpackages[0].Arch)
}

func TestParserParsesMultipleSubpackages(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		pkgver="1.2.3"
		subpackages="$pkgname-dbg
			$pkgname-dev
			$pkgname-doc
			$pkgname-foo-bar:foo_bar:noarch
			"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.Len(t, apkbuild.Subpackages, 4)
	assert.Equal(t, "test-package-dbg", apkbuild.Subpackages[0].Subpkgname)
	assert.Equal(t, "test-package-dev", apkbuild.Subpackages[1].Subpkgname)
	assert.Equal(t, "test-package-doc", apkbuild.Subpackages[2].Subpkgname)
	assert.Equal(t, "test-package-foo-bar", apkbuild.Subpackages[3].Subpkgname)
	assert.Equal(t, "foo_bar", apkbuild.Subpackages[3].SplitFunc)
	assert.Equal(t, "noarch", apkbuild.Subpackages[3].Arch)
}

func TestParsesSourceHash(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		pkgver="1.2.3"
		source="test-package-$pkgver.tar.gz::https://github.com/foo/test/archive/refs/tags/v$pkgver.tar.gz
			fix-test.patch
		"
		sha512sums="
		42ffe10890bdfcb60db0bf23eaf4e2ef4d643bd8f1014fc1ef51571837fcfc16b2d0a6c6e696cc4a75c7de5ae79941657dcdebf1a0ccc770ce67e95d0b783282  test-package-1.2.3.tar.gz
		a647cf494e1cfd60b49dca58d907fd38ed78346ae28b9ce29eace3a02e85b5f1489c137d075789d5d7f09d1d379adc89a6201e235690b8ab98c04a2423a171ed  fix-test.patch
		"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.Len(t, apkbuild.Sha512sums, 2)
	assert.Equal(t,
		"42ffe10890bdfcb60db0bf23eaf4e2ef4d643bd8f1014fc1ef51571837fcfc16b2d0a6c6e696cc4a75c7de5ae79941657dcdebf1a0ccc770ce67e95d0b783282",
		apkbuild.Sha512sums[0].Hash,
	)
	assert.Equal(t, "test-package-1.2.3.tar.gz", apkbuild.Sha512sums[0].Source)

	assert.Equal(t,
		"a647cf494e1cfd60b49dca58d907fd38ed78346ae28b9ce29eace3a02e85b5f1489c137d075789d5d7f09d1d379adc89a6201e235690b8ab98c04a2423a171ed",
		apkbuild.Sha512sums[1].Hash,
	)
	assert.Equal(t, "fix-test.patch", apkbuild.Sha512sums[1].Source)

}

func TestParsesInstallFields(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		install="$pkgname.pre-install"
		pkgusers="serviceuser"
		pkggroups="servicegroup"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)

	require.Len(t, apkbuild.Install, 1)
	assert.Equal(t, "test-package.pre-install", apkbuild.Install[0])

	require.Len(t, apkbuild.Pkgusers, 1)
	assert.Equal(t, "serviceuser", apkbuild.Pkgusers[0])

	require.Len(t, apkbuild.Pkggroups, 1)
	assert.Equal(t, "servicegroup", apkbuild.Pkggroups[0])
}

func TestParserTriggers(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		triggers="foo.trigger=path/a:path/b"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.Len(t, apkbuild.Triggers, 1)
	require.Equal(t, "foo.trigger", apkbuild.Triggers[0].Scriptname)
}

func TestParserBuilddir(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		pkgver="1.0.0"
		builddir="$srcdir/test"
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, expand.ListEnviron("srcdir=/tmp"))

	require.Nil(t, err)
	require.Equal(t, "/tmp/test", apkbuild.Builddir)
}

func TestParserFuncs(t *testing.T) {
	file := strings.NewReader(heredoc.Doc(`
		pkgname="test-package"
		
		build() {
			:
		}
	`))

	apkbuild, err := Parse(ApkbuildFile{
		PackageName: "community/test-package",
		Content:     file,
	}, nil)

	require.Nil(t, err)
	require.NotNil(t, apkbuild.Funcs)
	require.Len(t, apkbuild.Funcs, 1)
	require.NotNil(t, apkbuild.Funcs["build"])
	require.Nil(t, apkbuild.Funcs["package"])
}
