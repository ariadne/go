// This package is for backwards compatibility. Use
// gitlab.alpinelinux.org/alpine/go/releases instead.
package releases

import "gitlab.alpinelinux.org/alpine/go/releases"

type (
	Repo          = releases.Repo
	Release       = releases.Release
	ReleaseBranch = releases.ReleaseBranch
	Releases      = releases.Releases
)

var (
	NewFromBuffer = releases.NewFromBuffer
	NewFromURL    = releases.NewFromURL
	Fetch         = releases.Fetch
)
