// This package is for backwards compatibility. Use
// gitlab.alpinelinux.org/alpine/go/releases instead.
package apkbuild

import "gitlab.alpinelinux.org/alpine/go/apkbuild"

type (
	Fix          = apkbuild.Fix
	FixedVersion = apkbuild.FixedVersion
	Secfixes     = apkbuild.Secfixes
)

var (
	ParseSecfixes = apkbuild.ParseSecfixes
)
